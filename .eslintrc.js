module.exports = {
  env: {
    browser: true,
    es6: true,
  },
  globals: {
    ENV: true,
    it: true,
  },
  settings: {
    'import/resolver': {
      node: {
        extensions: ['.js', '.jsx', '.ts', '.tsx'],
      },
    },
  },
  extends: [
    'eslint:recommended',
    'airbnb',
  ],
  rules: {
    'import/extensions': [
      'error',
      'ignorePackages',
      {
        js: 'never',
        jsx: 'never',
        ts: 'never',
        tsx: 'never',
      },
    ],
    indent: ['error', 2], // 2 spaces indentation
    'linebreak-style': ['error', 'unix'], // \n instead of \r\n
    quotes: ['error', 'single'], // single quotes preferred
    semi: ['error', 'always'], // always use semicolons
    'brace-style': ['error', 'stroustrup', { allowSingleLine: true }], // Overrides Airbnb: statements on separated lines
    'max-len': ['error', {
      code: 120,
      tabWidth: 2,
      ignoreUrls: true,
      ignoreStrings: true,
      ignoreComments: true,
      ignoreTemplateLiterals: true,
      ignoreRegExpLiterals: true,
    }],
    'react/no-array-index-key': 0,
    'no-underscore-dangle': 0,
    'no-console': ['warn', { allow: ['warn', 'error'] }],
    'import/no-named-as-default-member': 0,
    camelcase: 0,
    // typeorm
    'class-methods-use-this': 0,
  },
};
