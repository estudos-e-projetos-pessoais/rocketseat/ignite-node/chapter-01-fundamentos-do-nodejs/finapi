import { v4 as uuid } from 'uuid';
import { omit } from 'lodash';
import { errorBuilder, controllerErrorHandler } from '../utils/errorUtils';

class AccountsService {
  constructor() {
    this.accounts = [];
  }

  /**
   *
   * @param {string} cpf
   * @param {string} name
   * @param {uuid} id
   * @param {array} statement
   */
  create({ cpf, name }) {
    const newAccount = {
      id: uuid(),
      name,
      cpf,
      statement: [],
    };

    if (this.customerAlreadyExists(cpf)) {
      throw errorBuilder(400, 'Customer already exists');
    }

    this.accounts = [...this.accounts, newAccount];
    return newAccount;
  }

  show(cpf) {
    const customer = this.getCustomerByCpf(cpf);
    return customer;
  }

  update({ cpf, name }) {
    const customer = this.getCustomerByCpf(cpf);
    customer.name = name;
    this.updateCustomerAccount(customer);
    return customer;
  }

  destroy(cpf) {
    this.accounts = this.accounts.filter((d) => d.cpf !== cpf);
    return true;
  }

  getStatement(cpf, params) {
    const { date } = params;
    const customer = this.getCustomerByCpf(cpf);

    if (!date) {
      return customer.statement;
    }

    const dateFormat = new Date(`${date} 00:00`);

    return customer.statement.filter((d) => (
      d.created_at.toDateString() === new Date(dateFormat).toDateString()
    ));
  }

  makeDeposit({ cpf, amount, description }) {
    const statementOperation = {
      description,
      amount,
      created_at: new Date(),
      type: 'credit',
    };

    this.updateCustomerStatement(cpf, statementOperation);

    return omit(statementOperation, 'type');
  }

  makeWithdraw({ cpf, amount, description }) {
    const balance = this.getCustomerBalance(cpf);

    if (balance < amount) {
      throw errorBuilder(400, 'Insufficient funds');
    }

    const statementOperation = {
      description,
      amount,
      created_at: new Date(),
      type: 'debit',
    };

    this.updateCustomerStatement(cpf, statementOperation);

    return omit(statementOperation, 'type');
  }

  // ----- PRIVATE -----

  getCustomerBalance(cpf) {
    const customer = this.getCustomerByCpf(cpf);

    return customer.statement.reduce((acc, cur) => {
      if (cur.type === 'credit') {
        return acc + cur.amount;
      }

      if (cur.type === 'debit') {
        return acc - cur.amount;
      }

      // skips invalid types
      return acc;
    }, 0);
  }

  updateCustomerStatement(cpf, statementOperation) {
    const customer = this.getCustomerByCpf(cpf);
    customer.statement = [...customer.statement, statementOperation];
    this.updateCustomerAccount(customer);
  }

  updateCustomerAccount(customer) {
    this.accounts = this.accounts.map((d) => (d.cpf === customer.cpf ? customer : d));
  }

  customerAlreadyExists(cpf) {
    return this.accounts.some((d) => d.cpf === cpf);
  }

  getCustomerByCpf(cpf) {
    const customer = this.accounts.find((d) => d.cpf === cpf);
    return customer;
  }

  verifyIfCustomerExistsMiddleware(req, res, next) {
    const { cpf } = req.headers;
    const customer = this.accounts.find((d) => d.cpf === cpf);

    if (!customer) {
      throw controllerErrorHandler(res, errorBuilder(400, 'Customer does not exist'));
    }

    return next();
  }
}

export default AccountsService;
