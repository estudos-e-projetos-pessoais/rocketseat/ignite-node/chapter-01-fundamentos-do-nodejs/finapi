import { controllerErrorHandler } from '../utils/errorUtils';

class AccountsController {
  constructor(accountsService) {
    this.service = accountsService;
  }

  create(req, res) {
    try {
      const account = this.service.create(req.body);
      return res.status(201).json(account);
    }
    catch (err) {
      return controllerErrorHandler(res, err);
    }
  }

  show(req, res) {
    try {
      const { cpf } = req.headers;
      const account = this.service.show(cpf);
      return res.status(200).json(account);
    }
    catch (err) {
      return controllerErrorHandler(res, err);
    }
  }

  update(req, res) {
    try {
      const { cpf } = req.headers;
      const { name } = req.body;
      const account = this.service.update({ cpf, name });
      return res.status(200).json(account);
    }
    catch (err) {
      return controllerErrorHandler(res, err);
    }
  }

  destroy(req, res) {
    try {
      const { cpf } = req.headers;
      this.service.destroy(cpf);
      return res.status(200).json({ message: 'Account destroyed' });
    }
    catch (err) {
      return controllerErrorHandler(res, err);
    }
  }

  getStatement(req, res) {
    try {
      const { cpf } = req.headers;
      const statement = this.service.getStatement(cpf, req.query);
      return res.status(200).json({ statement });
    }
    catch (err) {
      return controllerErrorHandler(res, err);
    }
  }

  makeDeposit(req, res) {
    try {
      const { cpf } = req.headers;
      const { amount, description } = req.body;
      const statementOperation = this.service.makeDeposit({ cpf, amount, description });
      return res.status(201).json({ deposit: statementOperation });
    }
    catch (err) {
      return controllerErrorHandler(res, err);
    }
  }

  makeWithdraw(req, res) {
    try {
      const { cpf } = req.headers;
      const { amount, description } = req.body;
      const statementOperation = this.service.makeWithdraw({ cpf, amount, description });
      return res.status(201).json({ withdraw: statementOperation });
    }
    catch (err) {
      return controllerErrorHandler(res, err);
    }
  }
}

export default AccountsController;
