export const errorBuilder = (code, message) => {
  const error = new Error(message);
  error.code = code;
  return error;
};

export const controllerErrorHandler = (res, err) => {
  if (!err.code) {
    console.log('ERR', err);
    return res.status(err.code || 500).send('Internal Server Error');
  }

  return res.status(err.code).json({ code: err.code, message: err.message });
};

export default {
  errorBuilder,
  controllerErrorHandler,
};
