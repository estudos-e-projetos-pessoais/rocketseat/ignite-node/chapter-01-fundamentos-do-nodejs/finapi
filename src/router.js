import { Router } from 'express';

import AccountsController from './controllers/AccountsController';
import AccountsService from './services/AccountsService';

import routes from './config/routes';

const router = Router();

// accounts
const accountsService = new AccountsService();
const accountsController = new AccountsController(accountsService);

router.post(routes.account, accountsController.create.bind(accountsController));

router.get(routes.account,
  accountsService.verifyIfCustomerExistsMiddleware.bind(accountsService),
  accountsController.show.bind(accountsController));

router.put(routes.account,
  accountsService.verifyIfCustomerExistsMiddleware.bind(accountsService),
  accountsController.update.bind(accountsController));

router.patch(routes.account,
  accountsService.verifyIfCustomerExistsMiddleware.bind(accountsService),
  accountsController.update.bind(accountsController));

router.delete(routes.account,
  accountsService.verifyIfCustomerExistsMiddleware.bind(accountsService),
  accountsController.destroy.bind(accountsController));

router.post(routes.deposit,
  accountsService.verifyIfCustomerExistsMiddleware.bind(accountsService),
  accountsController.makeDeposit.bind(accountsController));

router.post(routes.withdraw,
  accountsService.verifyIfCustomerExistsMiddleware.bind(accountsService),
  accountsController.makeWithdraw.bind(accountsController));

router.get(routes.statement,
  accountsService.verifyIfCustomerExistsMiddleware.bind(accountsService),
  accountsController.getStatement.bind(accountsController));

export default router;
