export default {
  account: '/account',
  statement: '/statement',
  deposit: '/deposit',
  withdraw: '/withdraw',
};
